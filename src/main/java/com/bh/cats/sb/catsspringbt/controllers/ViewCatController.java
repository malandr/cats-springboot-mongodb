package com.bh.cats.sb.catsspringbt.controllers;

import com.bh.cats.sb.catsspringbt.models.Cat;
import com.bh.cats.sb.catsspringbt.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@Controller
public class ViewCatController {

    private final Logger logger = Logger.getLogger(ViewCatController.class.getName());
    private final CatRepository catRepository;

    public ViewCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/view-all-cats")
    public String getAllCats(Model model) {

        Iterable<Cat> cats = catRepository.findAll();

        model.addAttribute("cats", cats);

        return "cats";
    }

    @GetMapping("/{id}/view")
    public String viewOneCat(@PathVariable String id, Model model) {

        Cat cat = catRepository.findCatById(id);

        model.addAttribute("cat", cat);

        return "cat";
    }


}
