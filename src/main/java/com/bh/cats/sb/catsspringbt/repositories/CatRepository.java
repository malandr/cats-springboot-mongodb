package com.bh.cats.sb.catsspringbt.repositories;

import com.bh.cats.sb.catsspringbt.models.Cat;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatRepository extends CrudRepository<Cat, Integer> {

    public List<Cat> findCatByName(String name);
    public Cat findCatById(String Id);

}
