package com.bh.cats.sb.catsspringbt.controllers;

import com.bh.cats.sb.catsspringbt.forms.CreateCatForm;
import com.bh.cats.sb.catsspringbt.models.Cat;
import com.bh.cats.sb.catsspringbt.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.logging.Logger;

@Controller
public class CreateCatController {

    private final Logger logger = Logger.getLogger(CreateCatController.class.getName());

    private final CatRepository catRepository;

    public CreateCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/create-cat")
    public String createCat(Model model) {

        model.addAttribute("createCatForm", new CreateCatForm());

        return "create-cat";
    }

    @PostMapping("/create-cat")
    public String createCatResult(@ModelAttribute CreateCatForm createCatForm) {

//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Cat cat = new Cat();

        cat.setName(createCatForm.getName());
        cat.setAge(createCatForm.getAge());
        cat.setColor(createCatForm.getColor());

        if (createCatForm.getColor().equals("Black")) {
            cat.setImagePath("../images/colors/black.jpg");
        } else if (createCatForm.getColor().equals("Red")) {
            cat.setImagePath("../images/colors/red.jpg");
        } else if (createCatForm.getColor().equals("Grey")) {
            cat.setImagePath("../images/colors/grey.jpg");
        } else if (createCatForm.getColor().equals("Three-Color")) {
            cat.setImagePath("../images/colors/three-color.jpg");
        } else if (createCatForm.getColor().equals("White")) {
            cat.setImagePath("../images/colors/white.jpg");
        } else if (createCatForm.getColor().equals("White and Grey")) {
            cat.setImagePath("../images/colors/white_and_grey.jpg");
        }

        logger.info(cat.getImagePath());

        catRepository.save(cat);

        return "cat-created";
    }
}
