package com.bh.cats.sb.catsspringbt.controllers;

import com.bh.cats.sb.catsspringbt.forms.FindCatForm;
import com.bh.cats.sb.catsspringbt.models.Cat;
import com.bh.cats.sb.catsspringbt.repositories.CatRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class FindCatController {

    private final CatRepository catRepository;

    public FindCatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("/find-cat")
    public String getCatByName(Model model) {

        model.addAttribute("findCatForm", new FindCatForm());

        return "find-cat";
    }

    @PostMapping("/find-cat")
    public String getCatByNameResult(@ModelAttribute FindCatForm findCatForm, Model model) {

        List<Cat> foundCats = catRepository.findCatByName(findCatForm.getCatName());

        model.addAttribute("foundCats", foundCats);

        return "found-cats";
    }
}
