package com.bh.cats.sb.catsspringbt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatsSpringbtApplication {

	public static void main(String[] args) {

		SpringApplication.run(CatsSpringbtApplication.class, args);
	}

}

