package com.bh.cats.sb.catsspringbt.forms;

public class FindCatForm {

    private String catName;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
